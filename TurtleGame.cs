﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleChallenge
{
    class TurtleGame
    {        
        /// <summary>
        /// Represents x dimension length of the input board
        /// </summary>
        int xboard { set; get; }

        /// <summary>
        /// Represents y dimension length of the input board
        /// </summary>
        int yboard { set; get; }

        /// <summary>
        /// Represents the turtle position
        /// </summary>
        string turtlePos { set; get; }

        /// <summary>
        /// Represents initial turtle direction
        /// </summary>
        string initialTurtleDir { set; get; }

        /// <summary>
        /// Represents location of the exit position.
        /// </summary>
        string exitPos { set; get; }

        /// <summary>
        /// Lists all the mine locations on the input board
        /// </summary>
        List<string> mineLocations { set; get; }

        /// <summary>
        /// Lists all input sequence of moves.
        /// </summary>
        List<string> lineMoves { set; get; }
                
        /// <summary>
        /// Turtle game constructor.Initializes the required game settings
        /// </summary>
        /// <param name="_xboard"></param>
        /// <param name="_yboard"></param>
        /// <param name="_turtlePos"></param>
        /// <param name="_initialTurtleDir"></param>
        /// <param name="_exitPos"></param>
        /// <param name="_mineLocations"></param>
        /// <param name="_lineMoves"></param>
        public TurtleGame(int _xboard, int _yboard, string _turtlePos, string _initialTurtleDir, string _exitPos, List<string> _mineLocations, List<string> _lineMoves)
        {
            xboard = _xboard;
            yboard = _yboard;
            turtlePos = _turtlePos;
            initialTurtleDir = _initialTurtleDir;
            exitPos = _exitPos;
            mineLocations = _mineLocations;
            lineMoves = _lineMoves;
        }

        /// <summary>
        /// Method to retrieve results for each execution path retrieved from second input file.
        /// </summary>
        public void getMoveSequenceResults()
        {
            int cnt = 0;
            string status = "";
            string curPos = turtlePos;
            string curDir = initialTurtleDir;
            foreach (string strmove in lineMoves)
            {
                List<string> lstMoves = strmove.Split(',').ToList();
                foreach (string move in lstMoves)
                {
                    int xCurPos = Convert.ToInt32(curPos.Split(',')[0]);
                    int yCurPos = Convert.ToInt32(curPos.Split(',')[1]);
                    if (move == "m" && curDir == "e" && xCurPos < xboard - 1)
                    {
                        xCurPos++;
                    }
                    else if (move == "m" && curDir == "w" && xCurPos > 0)
                    {
                        xCurPos--;
                    }
                    else if (move == "m" && curDir == "s" && yCurPos < yboard - 1)
                    {
                        yCurPos++;
                    }
                    else if (move == "m" && curDir == "n" && xCurPos > 0)
                    {
                        yCurPos--;
                    }
                    else if (move == "r" && curDir == "n")
                    {
                        curDir = "e";
                    }
                    else if (move == "r" && curDir == "e")
                    {
                        curDir = "s";
                    }
                    else if (move == "r" && curDir == "s")
                    {
                        curDir = "w";
                    }
                    else if (move == "r" && curDir == "w")
                    {
                        curDir = "n";
                    }

                    curPos = xCurPos + "," + yCurPos;
                    if (curPos == exitPos)
                    {
                        status = "Success !";
                        break;
                    }
                    else if (mineLocations.Contains(curPos))
                    {
                        status = "Mine hit !";
                        break;
                    }
                    else
                    {
                        status = "Still in danger !";

                    }
                }
                curDir = initialTurtleDir;
                curPos = turtlePos;
                cnt++;
                Console.WriteLine("Sequence" + cnt.ToString() + ":" + status.ToString());

            }
            Console.Read();
        }
    }
}
