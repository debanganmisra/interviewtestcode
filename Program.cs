﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Fetching input datas

            List<string> lineSettings = File.ReadAllLines("D:\\Debangan\\Work\\TurtleChallenge\\Input\\input1.txt").ToList();

            //board dimensions
            List<string> boardDimensions = lineSettings[0].Split(',').ToList();
            int xboard = Convert.ToInt32(boardDimensions[0]);
            int yboard = Convert.ToInt32(boardDimensions[1]);
                
            //Initial turtle position
            string turtlePos = lineSettings[1];
            string initialTurtleDir = lineSettings[2];

            //exit point position
            string exitPos = lineSettings[3];


            //mine locations
            List<string> mineLocations = new List<string>();
            if (lineSettings.Count > 4)
            {
                for (int i = 4; i < lineSettings.Count; i++)
                {
                    mineLocations.Add(lineSettings[i].ToString());
                }
            }

            List<string> lineMoves = File.ReadAllLines("D:\\Debangan\\Work\\TurtleChallenge\\Input\\input2.txt").ToList();

            #endregion

            #region TurtleGameInitialization and Execution
            TurtleGame objTurtleGame = new TurtleGame(xboard,yboard,turtlePos,initialTurtleDir,exitPos,mineLocations,lineMoves);
            objTurtleGame.getMoveSequenceResults();
            #endregion

        }


    }
}
